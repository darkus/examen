-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-12-2018 a las 02:01:14
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordenadas`
--

CREATE TABLE IF NOT EXISTS `coordenadas` (
  `id_coord` int(11) NOT NULL,
  `longitud_coord` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `latitud_coord` varchar(250) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `coordenadas`
--

INSERT INTO `coordenadas` (`id_coord`, `longitud_coord`, `latitud_coord`) VALUES
(16, ' -66.17454506986695', '-17.380932996394716'),
(17, ' -66.16827942960816', '-17.380564393637705'),
(18, ' -66.17016770475465', '-17.38347223966173'),
(19, ' -66.16407372587281', '-17.382530266430987'),
(20, ' -66.16737820737916', '-17.383758926204443'),
(21, ' -66.17943741911012', '-17.382980775972307'),
(22, ' -66.30301215284425', '-17.36868679647783'),
(23, ' -66.26661994093018', '-17.36966979927496'),
(24, ' -66.21374823682862', '-17.36934213226205'),
(25, ' -66.34215094678956', '-17.36704844676032'),
(26, ' -66.17636897199708', '-17.38269408821068');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `coordenadas`
--
ALTER TABLE `coordenadas`
  ADD PRIMARY KEY (`id_coord`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `coordenadas`
--
ALTER TABLE `coordenadas`
  MODIFY `id_coord` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
