<!DOCTYPE html >
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Using MySQL and PHP with Google Maps</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <style>
      
      .map {
        height: 100%;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>

  <body>

  <div class="" style=" font-size: 20px;">
        <nav class="navbar navbar-expand-md navbar-light" style="background-color:rgb(122, 122, 248)">
        <a class="navbar-brand" href="#">Map</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
            <a class="nav-item nav-link" href="examen.php">Mi mapa<span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="coordenadas.php">Mis Coordenadas</a>
            </div>  
        </div>
        </nav>
    </div >
<div>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRqo4qwqHHrtbY9aL6vYYDw1GOhkK97MQ"></script>

<div class="col-md-12 " >
       
        
        <div class="container mt-5">
            <div class="row ">
                <h1 class="text-center">Coordenadas</h1>
                
                <div class="form-group col-mid-4">
                    
                    <label for="latitud">latitud</label>
                    <input type="text" class="form-control" id="latitud" name="latitud">
                </div>
                <div class="form-group col-mid-4">
                    <label for="longitud">longitud</label>
                    <input type="text" class="form-control" id="longitud" name="longitud">
                </div>
              

           

            </div>
            
        </div>
     
        <div class="col-md-12 col-sm-12 map" id="map-canvas" style="height:200px;"></div>
    
</div>
 <script>
      function initMap() {
        var mapa = new google.maps.Map(document.getElementById('map-canvas') ,{
 
          zoom: 15,
          center: {lat: -17.3824074, lng: -66.1711333}
          });
          google.maps.event.addListener(mapa, "click", function(event){
          var cordenadas=event.latLng.toString();
          cordenadas=cordenadas.replace("(","");
          cordenadas=cordenadas.replace(")","");
          var lista=cordenadas.split(",");
          var latitudd=lista[0];
          var longitudd=lista[1];

          lat=document.getElementById("latitud").value=latitudd;
          lon=document.getElementById("longitud").value=longitudd;
          caj1=document.getElementById("latitud").value;
          caj2=document.getElementById("longitud").value;
          $.ajax({
         type: "GET",
      dataType: 'json',
      data: {'latitud':caj1, 
             'longitud':caj2}, 
      url: "configura/guardar_coorde.php",
      success : function(data) {

          console.log(data);

      }
  });

          var direccion = new google.maps.LatLng(lista[0], lista[1]);
          lat.innerHTML="latitudd";
          lon.innerHTML="longitudd";
          var marcador = new google.maps.Marker({
              position:direccion,
              map: mapa,
              draggable:false
            });
              google.maps.event.addListener(marcador,click,function(){

              });
            marcador.setMap(mapa);
          });
          

      } 

        

    
    </script>


    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap">
    </script>
  </body>
</html>